import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;

import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.log4j.Logger;


import java.io.IOException;

/**
 * Created by joseph on 16/7/15.
 */
public class MapReduceQb {
    static Logger logger= Logger.getLogger(MapReduceQb.class);
    public static void main(String args[]) {
        System.out.println("started");
        System.out.println("started ");
        Configuration config = HBaseConfiguration.create();
        Job job = null;
        try {
            job = new Job(config,"ExampleReadWrite");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("config created");
        job.setJarByClass(MySummaryJob.class);    // class that contains mapper

        JobConf conf = new JobConf(MySummaryJob.class);
        Scan scan = new Scan();
        scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
        scan.setCacheBlocks(false);  // don't set to true for MR jobs
// set other scan attrs
        System.out.println("scan ");
        try {
            TableMapReduceUtil.initTableMapperJob(
                    "unstructured_data",      // input table
                    scan,	          // Scan instance to control CF and attribute selection
                    MySummaryJob.MyMapper.class,   // mapper class
                    ImmutableBytesWritable.class,	          // mapper output key
                    Put.class,	          // mapper output value
                    job);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            TableMapReduceUtil.initTableReducerJob(
                    "example_table",      // output table
                    null,             // reducer class
                    job);
        } catch (IOException e) {
            e.printStackTrace();
        }
        job.setNumReduceTasks(0);
        System.out.println("job.started");
        boolean b = false;
        try {
            System.out.println("gonna  submit the job");

            System.out.println("gonna  start wait");
            b = job.waitForCompletion(false);
            System.out.println("job waiting over");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (!b) {
            try {
                throw new IOException("info with job!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}}