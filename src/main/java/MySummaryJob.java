import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.log4j.Logger;


import java.io.IOException;

/**
 * Created by joseph on 16/7/15.
 */
public class MySummaryJob
{
public static class MyMapper extends TableMapper<ImmutableBytesWritable, Put>  {
static  Logger logger= Logger.getLogger(MyMapper.class);
    public void map(ImmutableBytesWritable row, Result value, Context context) throws IOException, InterruptedException {
        // this example is just copying the data from the source table...
        logger.info("in mapper");
        context.write(row, resultToPut(row,value));
        logger.info("write done");
    }

    private static Put resultToPut(ImmutableBytesWritable key, Result result) throws IOException {
        logger.info("in mapper");
        Put put = new Put(key.get());
        for (KeyValue kv : result.raw()) {
            put.add(kv);
        }
        logger.info("done putting  key");
        return put;
    }
}




}
